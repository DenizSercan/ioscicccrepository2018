//
//  ViewController.swift
//  MovieApp
//
//  Created by admin on 2018-10-24.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var movieTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.movieTableView.delegate = self
        self.movieTableView.dataSource=self;
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    

    //-- MARK delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0
        {
            return 10
        }
        else {
            return 20
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var tableviewCell:UITableViewCell?
        
        if indexPath.row%2 == 0
        {
            tableviewCell = tableView.dequeueReusableCell(withIdentifier: "CustomMovieCellIdentifier") as? MovieTableViewCell
            
            if tableviewCell==nil
            {
                tableviewCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "CustomMovieCellIdentifier") as? MovieTableViewCell
            }
        }
        else
        {
            tableviewCell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell
            
            if tableviewCell==nil
            {
                tableviewCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "movieCell")
            }
        }
     
        
        tableviewCell?.detailTextLabel?.text = "Movie Detail"
        tableviewCell?.textLabel?.text = "Text Label"
        tableviewCell?.textLabel?.textColor = .white
        tableviewCell?.textLabel?.backgroundColor = .yellow
        tableviewCell?.imageView?.image = UIImage(named: "star1")
        
        return tableviewCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section==0
        {
            return 50
        }
        else
        {
            return 80
        }
//        if indexPath.row%2==0
//        {
//            return 60
//        }
//        else {
//            return 100
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header: MovieSectionHeaderView =  MovieSectionHeaderView.instanceFromNib() as! MovieSectionHeaderView
        header.frame = CGRect(x: 0, y: 0, width: self.movieTableView.frame.size.width, height: 40)
        return header
    }
}

